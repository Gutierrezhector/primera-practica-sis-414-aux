
# Primera practica

Describir para que sirven los siguientes comandos de Git

****Git init:****  Inicia un nuevo repositorio Git en un directorio local

****Git clone:****  Clona un repositorio Git existente en un nuevo directorio local.

****Git add:****  Agrega archivos al área de preparación para el próximo commit.

****Git commit:****  Guarda los cambios en el repositorio local con un mensaje descriptivo.

****Git status:****  Muestra el estado actual del repositorio y los archivos.

****Git log:****  Muestra el historial de commits del repositorio.

****Git pull:****  Descarga y fusiona los cambios remotos en el repositorio local.

****Git push:****   Sube los commits locales al repositorio remoto.

****Git branch:****  Lista, crea y elimina ramas en el repositorio.

****Git checkout:****  Cambia entre ramas o restaura archivos.

****Git merge:****  Fusiona una rama con otra en el repositorio.

****Git remote:****  Administra repositorios remotos conectados.

****Git diff:****  Muestra las diferencias entre archivos o commits.

****Git git tag:****  Administra etiquetas para marcar puntos específicos en la historia del repositorio.
